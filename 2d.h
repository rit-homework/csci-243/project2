#ifndef D_H
#define D_H

#include "position.h"
#include <stdbool.h>

/// Makes a 2D array of booleans.
bool **make_2d_bool(size_t rows, size_t cols, bool defaultValue);

/// Makes a 2D array of position elements.
Position **make_2d_Position(size_t rows, size_t cols, Position defaultPosition);

/// Frees a 2d array of elements.
void free_2d(void **array_2d, size_t rows);

#endif
