#include "list.h"

List *new_list() {
    List *list = malloc(sizeof(List));
    list->length = 0;
    list->cap = 1;
    list->values = malloc(sizeof(int *));

    return list;
}

void free_list(List *list) {
    // Free Sub-Arrays
    for (size_t i = 0; i < list->length; i++) {
        free(list->values[i]);
    }

    // Free Root Array
    free(list->values);
    free(list);
}

int append_list(List *list, void *value) {
    if (list->cap == list->length) {
        size_t new_cap = list->cap * 2;

        void **new_vals = realloc(list->values, new_cap * sizeof(void *));
        if (new_vals == NULL) {
            // Allocation Failed
            return 0;
        }

        list->values = new_vals;
        list->cap = new_cap;
    }

    // Append Value
    list->values[list->length] = value;
    list->length++;

    return 1;
}
