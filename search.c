#define _GNU_SOURCE
#include <stdbool.h>
#include <string.h>

#include "2d.h"
#include "board.h"
#include "position.h"
#include "queue.h"
#include "search.h"

/// The position from which the search starts.
static const Position START_POSITION = {.x = 0, .y = 0, .is_empty = false};

/// Gets the goal position for the board.
static Position get_goal_position(const Board *board) {
    Position position = {
        .is_empty = false,
        .x = board->cols - 1,
        .y = board->rows->length - 1,
    };

    return position;
}

/// Given the trail matrix, builds a list of elements which are a part of the
/// path. The list starts at the end (bottom right exit) and ends at the
/// beginning (top left corner). The returned list is completely owned by the
/// caller.
static List *build_path(Position **solution, Board *board) {
    List *positions = new_list();
    if (positions == NULL) {
        return NULL;
    }

    Position position = get_goal_position(board);
    while (!position.is_empty) {
        // Copy Position to Heap
        Position *heap_position = malloc(sizeof(Position));
        if (heap_position == NULL) {
            return NULL;
        }

        Position next_position = solution[position.y][position.x];

        memcpy(heap_position, &next_position, sizeof(Position));

        // Add Position
        append_list(positions, heap_position);

        // Move
        position = next_position;
    }

    return positions;
}

List *find_solution(Board *board) {
    int *row = board->rows->values[START_POSITION.y];
    int symbol = row[START_POSITION.x];
    if (symbol == 1) {
        return NULL;
    }

    // Initialize History Map
    Position **trail =
        make_2d_Position(board->rows->length, board->cols, EMPTY_POSITION);
    if (trail == NULL) {
        return NULL;
    }

    // Initialize Visitation History
    bool **closed_set = make_2d_bool(board->rows->length, board->cols, false);
    if (closed_set == NULL) {
        return NULL;
    }

    bool **open_set = make_2d_bool(board->rows->length, board->cols, false);
    if (open_set == NULL) {
        return NULL;
    }

    const Position goal_position = get_goal_position(board);

    // Initialize Continuation Queue
    Queue *queue = new_queue();
    enqueue(queue, START_POSITION);

    // Mark Source as Visited
    closed_set[START_POSITION.y][START_POSITION.x] = true;

    while (true) {
        // Pop Position From Queue
        Position position = dequeue(queue);
        if (position.is_empty) {
            // Reached the end of the queue without reaching the goal.
            break;
        }

        // Check Whether Goal
        if (equals_position(position, goal_position)) {
            free_2d((void **)closed_set, board->rows->length);
            free_2d((void **)open_set, board->rows->length);

            List *path = build_path(trail, board);
            free_2d((void **)trail, board->rows->length);

            free_queue(queue);

            return path;
        }

        // Get Successors
        for (int i = 0; i < directions_count; i++) {
            Direction direction = directions[i];

            Position next_position = add_to_position(position, direction);
            bool bounds_ok =
                within_bounds(next_position, board->cols, board->rows->length);

            if (!bounds_ok) {
                // Skip Out Of Bounds
                continue;
            }

            if (open_set[next_position.y][next_position.x]) {
                // Skip Existing Positions
                continue;
            }

            // Check If Already Visited (found successors of)
            bool has_visited = closed_set[next_position.y][next_position.x];
            if (has_visited) {
                // Already Visited, Skip
                continue;
            }

            int *row = board->rows->values[next_position.y];
            int symbol = row[next_position.x];
            if (symbol == 1) {
                // Can't move in this direction.
                continue;
            }

            // Mark Spot With Trail
            Position from_position = position;
            from_position.is_empty = false;
            trail[next_position.y][next_position.x] = from_position;

            enqueue(queue, next_position);
            open_set[next_position.y][next_position.x] = true;
        }

        // Update Visitation History
        closed_set[position.y][position.x] = true;
    }

    // The goal was never reached.
    free_queue(queue);
    free_2d((void **)closed_set, board->rows->length);
    free_2d((void **)open_set, board->rows->length);
    free_2d((void **)trail, board->rows->length);

    return NULL;
}
