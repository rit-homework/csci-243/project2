#ifndef DIRECTIONS_H
#define DIRECTIONS_H

/// A descriptor for a direction.
typedef struct {
    /// An offset in the y direction.
    int offset_y;

    /// An offset in the x direction.
    int offset_x;
} Direction;

/// All directions where neighbors may exist.
extern const Direction directions[];

/// The number of elements in the directions array.
extern const int directions_count;

#endif
