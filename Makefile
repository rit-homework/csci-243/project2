CFLAGS=-Wall -Wextra -pedantic -std=c99 -Werror -fno-omit-frame-pointer

all: mopsolver

mopsolver: args.o directions.o list.o position.o queue.o mopsolver.o board.o search.o 2d.o
	$(CC) -o $@ $^

fix-formatting:
	clang-format -i *.c *.h

test: mopsolver
	./test.sh

clean:
	rm -rf mopsolver *.o perf callgrind

.PHONY: clean fix-formatting test
