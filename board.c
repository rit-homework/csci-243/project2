#define _GNU_SOURCE
#include "board.h"
#include "2d.h"
#include "position.h"
#include <stdio.h>

/**
 * Converts a digit to an integer. Only accepts 0 and 1.
 *
 * @param c The digit character to convert.
 * @return The integer version of that digit or -1 if not known.
 */
static int digit_to_int(char c) {
    if (c == '0') {
        return 0;
    } else if (c == '1') {
        return 1;
    } else {
        return -1;
    }
}

/**
 * Converts a number to its corresponding symbol.
 *
 * @param i The number.
 * @return A character representation of the number.
 */
static char int_to_symbol(int i) {
    if (i == 1) {
        return 'O';
    } else {
        return ' ';
    }
}

/**
 * Prints the top or bottom side of a board.
 *
 * @param size of the row.
 */
static void print_filled_edge(size_t size) {
    for (size_t i = 0; i < (size + 1); i++) {
        printf("OO");
    }

    printf("O");

    printf("\n");
}

void free_board(Board *board) {
    free_list(board->rows);
    free(board);
}

void print_input_matrix(const Board *board) {
    for (size_t rowIdx = 0; rowIdx < board->rows->length; rowIdx++) {
        int *row = board->rows->values[rowIdx];

        for (size_t colIdx = 0; colIdx < board->cols; colIdx++) {
            int tile = row[colIdx];

            printf("%d", tile);

            if (colIdx < board->cols - 1) {
                printf(" ");
            }
        }

        printf("\n");
    }
}

void print_board(Board *board, List *path) {
    bool **history = make_2d_bool(board->rows->length, board->cols, false);
    if (history == NULL) {
        fprintf(stderr, "Failed to Allocate History");
        return;
    }

    if (path != NULL) {
        // Populate History
        for (size_t i = 0; i < path->length; i++) {
            Position *pos = path->values[i];

            history[pos->y][pos->x] = true;
        }

        history[board->rows->length - 1][board->cols - 1] = true;
    }

    // First Row
    print_filled_edge(board->cols);
    for (size_t rowIdx = 0; rowIdx < board->rows->length; rowIdx++) {
        // Print Left Wall
        char left_wall_symbol = int_to_symbol(1);
        if (rowIdx == 0) {
            left_wall_symbol = int_to_symbol(0);

            if (path != NULL) {
                left_wall_symbol = '+';
            }
        }
        printf("%c", left_wall_symbol);

        // Print Row
        int *row = board->rows->values[rowIdx];
        for (size_t colIdx = 0; colIdx < board->cols; colIdx++) {
            char symbol = int_to_symbol(row[colIdx]);
            if (symbol == ' ' && history != NULL && history[rowIdx][colIdx]) {
                symbol = '+';
            }

            printf(" %c", symbol);
        }

        char right_wall_symbol = int_to_symbol(1);
        if (rowIdx == board->rows->length - 1) {
            right_wall_symbol = int_to_symbol(0);

            if (path != NULL) {
                right_wall_symbol = '+';
            }
        }

        // Print Right Wall
        printf(" %c\n", right_wall_symbol);
    }

    // Last Row
    print_filled_edge(board->cols);
    free_2d((void **)history, board->rows->length);
}

Board *read_board(FILE *file) {
    Board *board = malloc(sizeof(Board));

    board->rows = new_list();
    if (board->rows == NULL) {
        fprintf(stderr, "Failed to Allocate List\n");
        return NULL;
    }

    // The buffer allocated by getline.
    char *line = NULL;

    // The size of the buffer allocated by getline.
    size_t len = 0;

    // The length of first line (and every other line).
    ssize_t line_size = -1;

    // Read Lines
    while (true) {
        // Length of the Line (including newline charcter)
        ssize_t line_len = getline(&line, &len, file);
        if (line_len == -1) {
            // Reached End of File
            break;
        }

        if (line_size < 0) {
            // First Line
            line_size = line_len;
        }

        if (line_size != line_len) {
            // Invariant Assumption Violated, All Lines Aren't the Same Length
            fprintf(stderr, "Invariant Assumption Violated, All Lines Aren't "
                            "the Same Length\n");
            return NULL;
        }

        // Number of elements in the line.
        size_t elems_count = (size_t)line_size / 2;
        board->cols = elems_count;

        // Array Of Numbers In Row
        int *elems = malloc(sizeof(int) * elems_count);

        // Populate Row
        for (size_t i = 0; i < elems_count; i++) {
            char c = line[i * 2];

            int parsed = digit_to_int(c);
            if (parsed == -1) {
                fprintf(stderr, "Invalid Token Encountered\n");
                return NULL;
            }

            elems[i] = parsed;
        }

        append_list(board->rows, elems);
    }

    if (line_size == -1) {
        // No Lines Were Read
        fprintf(stderr, "No Lines Were Read");

        return NULL;
    }

    free(line);

    return board;
}
