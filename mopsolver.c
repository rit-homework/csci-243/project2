#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include "args.h"
#include "board.h"
#include "search.h"

/// The main method of the program. Parses arguments, solves maze and prints
/// solution.
int main(int argc, char *argv[]) {
    // Parse Arguments
    Arguments *args = parse_arguments(argc, argv);
    if (args == NULL) {
        return EXIT_FAILURE;
    }

    // Print Help Message
    if (args->print_help_message) {
        printf("%s", HELP_MESSAGE);
        return EXIT_SUCCESS;
    }

    // Parse Board
    Board *board = read_board(args->input);
    if (board == NULL) {
        return EXIT_FAILURE;
    }

    // Display Input Matrix if Necessary
    if (args->print_input_matrix) {
        printf("Read this matrix:\n");

        print_input_matrix(board);
    }

    // Build Trail
    List *path = find_solution(board);
    if (args->print_shortest_length) {
        if (path != NULL) {
            printf("Solution in %ld steps.\n", path->length);
        } else {
            printf("No solution.\n");
        }
    }

    if (args->pretty_print_input_board) {
        print_board(board, NULL);
    }

    if (args->print_solution) {
        if (path != NULL) {
            print_board(board, path);
        } else {
            printf("No solution.\n");
        }
    }

    free_arguments(args);
    free_board(board);
    if (path != NULL) {
        free_list(path);
    }

    return EXIT_SUCCESS;
}
