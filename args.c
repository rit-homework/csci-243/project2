#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "args.h"

/**
 * The getopt options string of all the flags this program accepts.
 */
static char OPTIONS[] = {HELP_MESSAGE_FLAG,
                         BORDERS_FLAG,
                         SOLUTION_STEPS_FLAG,
                         MATRIX_FLAG,
                         PRINT_SOLUTION_FLAG,
                         OUTFILE_FLAG,
                         ':',
                         INFILE_FLAG,
                         ':'};

const char *HELP_MESSAGE =
    "USAGE:\n"
    "mopsrunner [-bspmh] [-i INFILE] [-o OUTFILE]\n"
    "\n"
    "Options:\n"
    "-h Print this helpful message to stdout.\n"
    "-b Add borders and pretty print.  (Default off.)\n"
    "-s Print shortest solution steps. (Default off.)\n"
    "-m Print matrix after reading.    (Default off.)\n"
    "-p Print solution with path.      (Default off.)\n"
    "-i INFILE Read maze from INFILE.  (Default stdin.)\n"
    "-o OUTFILE Write maze to OUTFILE. (Default stdout.)\n";

/**
 * Initializes an argument struct with default values.
 *
 * @return A newly initialized argument struct with default values.
 */
static Arguments *new_arguments() {
    Arguments *arguments = malloc(sizeof(Arguments));
    if (arguments == NULL) {
        return NULL;
    }

    arguments->print_help_message = false;
    arguments->pretty_print_input_board = false;
    arguments->print_shortest_length = false;
    arguments->print_input_matrix = false;
    arguments->print_solution = false;
    arguments->input = stdin;
    arguments->output = stdout;

    return arguments;
}

int free_arguments(Arguments *args) {
    int ret = fclose(args->input);
    if (ret != 0) {
        fprintf(stderr, "Failed to Close Input File\n");
        return ret;
    }

    ret = fclose(args->output);
    if (ret != 0) {
        fprintf(stderr, "Failed to Close Output File\n");
        return ret;
    }

    free(args);
    return 0;
}

Arguments *parse_arguments(int argc, char *argv[]) {
    Arguments *args = new_arguments();
    if (args == NULL) {
        fprintf(stderr, "Failed to Allocate Arguments\n");
        return NULL;
    }

    int khar = 0;

    while ((khar = getopt(argc, argv, OPTIONS)) != -1) {
        switch (khar) {
        case HELP_MESSAGE_FLAG:
            args->print_help_message = true;
            break;

        case BORDERS_FLAG:
            args->pretty_print_input_board = true;
            break;

        case SOLUTION_STEPS_FLAG:
            args->print_shortest_length = true;
            break;

        case MATRIX_FLAG:
            args->print_input_matrix = true;
            break;

        case PRINT_SOLUTION_FLAG:
            args->print_solution = true;
            break;

        case INFILE_FLAG:
            args->input = fopen(optarg, "r");
            if (args->input == NULL) {
                fprintf(stderr, "Failed to Open Input File\n");
                return NULL;
            }

            break;

        case OUTFILE_FLAG:
            args->output = fopen(optarg, "w");
            if (args->input == NULL) {
                fprintf(stderr, "Failed to Open Output File\n");
                return NULL;
            }
            break;

        default:
            fprintf(stderr, "Encountered Invalid Argument\n");
            return NULL;
        }
    }

    return args;
}
