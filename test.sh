#!/bin/bash

# Fail on First Error
set -e

# [ ! -d ./FlameGraph ] && git clone git@github.com:brendangregg/FlameGraph.git

function check_memory {
  valgrind \
    --leak-check=full \
    --error-exitcode=1 \
    ./mopsolver -spm -i "examples/$1"
}

function check_callgrind {
  mkdir -p callgrind

  valgrind \
    --tool=callgrind \
    --callgrind-out-file="callgrind/$1" \
    ./mopsolver -s -i "examples/$1"
}

function check_perf {
  mkdir -p perf

  perf record -g --output="perf/$1.perf" ./mopsrunner -s -i "examples/$1"
  perf script --input="perf/$1.perf" > "perf/$1.txt"

  ./FlameGraph/stackcollapse-perf.pl \
    "perf/$1.txt" | ../FlameGraph/flamegraph.pl > "perf/$1.svg"
}

# Check For Everything Except Performance Tests
items=(
  blocked1
  rand_10k
  empty10x20
  empty4x4
  empty8x8
  full10x20
  full4x4
  full8x8
  rand0
  rand1
  rand2
  zigzag10x20
  zigzag4x4
  zigzag8x8
)

for item in ${items[@]}
do
  echo "Profiling $item"
  echo "Checking Memory"
  check_memory ${item}

  # echo "Performance"
  # check_perf ${item}
done

# ./mopsrunner -s -i "./examples/rand_100m"
# ./mopsrunner -s -i "./examples/rand_16m"
