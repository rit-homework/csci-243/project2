#define _GNU_SOURCE

#include "position.h"

Position **make_2d_Position(size_t rows, size_t cols,
                            Position defaultPosition) {
    Position **trail = malloc(sizeof(Position *) * rows);
    if (trail == NULL) {
        return NULL;
    }

    for (size_t rowIdx = 0; rowIdx < rows; rowIdx++) {
        trail[rowIdx] = malloc(sizeof(Position) * cols);
        if (trail[rowIdx] == NULL) {
            return NULL;
        }

        for (size_t colIdx = 0; colIdx < cols; colIdx++) {
            trail[rowIdx][colIdx] = defaultPosition;
        }
    }

    return trail;
}

bool **make_2d_bool(size_t rows, size_t cols, bool defaultValue) {
    bool **history = malloc(sizeof(bool *) * rows);
    if (history == NULL) {
        return NULL;
    }

    for (size_t rowIdx = 0; rowIdx < rows; rowIdx++) {
        history[rowIdx] = malloc(sizeof(bool) * cols);
        if (history[rowIdx] == NULL) {
            return NULL;
        }

        for (size_t colIdx = 0; colIdx < cols; colIdx++) {
            history[rowIdx][colIdx] = defaultValue;
        }
    }

    return history;
}

void free_2d(void **array_2d, size_t rows) {
    for (size_t rowIdx = 0; rowIdx < rows; rowIdx++) {
        void *row = array_2d[rowIdx];

        free(row);
    }

    free(array_2d);
}
