#define _GNU_SOURCE
#include "position.h"

const Position EMPTY_POSITION = {.is_empty = true};

bool equals_position(Position a, Position b) {
    return a.is_empty == b.is_empty && a.x == b.x && a.y == b.y;
}

Position add_to_position(Position start, Direction offset) {
    Position position = {.is_empty = false,
                         .x = start.x + offset.offset_x,
                         .y = start.y + offset.offset_y};

    return position;
}

bool within_bounds(Position position, size_t max_x, size_t max_y) {
    return position.x < max_x && position.y < max_y;
}
