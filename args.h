#ifndef ARGS_H
#define ARGS_H

#include <stdbool.h>

/**
 * Holds configuration information about how the program will run.
 */
typedef struct {
    /**
     * Whether or not the help message should be printed. By default, this is
     * false.
     */
    bool print_help_message;

    /**
     * The flag associated with the print_help_message field.
     */
#define HELP_MESSAGE_FLAG 'h'

    /**
     * Whether or not to add borders and pretty print. By default, this is
     * false.
     */
    bool pretty_print_input_board;

    /**
     * The flag associated with the print_borders field.
     */
#define BORDERS_FLAG 'b'

    /**
     * Prints the shortest solution steps. By default, this is disabled.
     */
    bool print_shortest_length;

    /**
     * The flag associated with the print_shortest field.
     */
#define SOLUTION_STEPS_FLAG 's'

    /**
     * Print input matrix after reading. By default, this is disabled.
     */
    bool print_input_matrix;

    /**
     * The flag associated with the print_input_matrix field.
     */
#define MATRIX_FLAG 'm'

    /**
     * Print solution with path. By default, this is disabled.
     */
    bool print_solution;

    /**
     * The flag associated with the print_solution_flag field.
     */
#define PRINT_SOLUTION_FLAG 'p'

    /**
     * The input file to read the problem information from. By default, this is
     * stdin.
     */
    FILE *input;

    /**
     * The flag associated with the input field.
     */
#define INFILE_FLAG 'i'

    /**
     * The output file to write the maze to. By default, this is standard out.
     */
    FILE *output;

    /**
     * The flag associated with the output field.
     */
#define OUTFILE_FLAG 'o'
} Arguments;

extern const char *HELP_MESSAGE;

/**
 * Close Files and Frees Arguments struct.
 *
 * @param args the argument to free.
 */
int free_arguments(Arguments *args);

/**
 * Parses arguments from main into structured data.
 *
 * @param argc The number of arguments to parse.
 * @param argv An array of c-strings.
 * @return The parsed arguments.
 */
Arguments *parse_arguments(int argc, char *argv[]);

#endif