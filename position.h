#ifndef POS_H
#define POS_H

#include "directions.h"
#include <stdbool.h>
#include <stdlib.h>

/**
 * A two dimensional position.
 */
typedef struct {
    size_t x;
    size_t y;

    bool is_empty;
} Position;

/// Returns true when the two positions share all of the same fields.
bool equals_position(Position a, Position b);

/// Adds an offset to a position.
Position add_to_position(Position start, Direction offset);

/// Checks whether a position's coordinates are between 0..max_x and 0..max_y.
bool within_bounds(Position position, size_t max_x, size_t max_y);

/// An empty position. is_empty is set to true.
extern const Position EMPTY_POSITION;

#endif
