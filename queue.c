#include "queue.h"
#include <stdio.h>

Queue *new_queue() {
    Queue *queue = malloc(sizeof(Queue));
    if (queue == NULL) {
        return NULL;
    }

    queue->head = NULL;
    queue->tail = NULL;

    return queue;
}

/**
 * Allocates and initializes a new queue node.
 *
 * @return A newly allocated and initialized queue node.
 */
static QueueNode *new_queue_node() {
    QueueNode *node = malloc(sizeof(QueueNode));
    if (node == NULL) {
        return NULL;
    }

    node->position = EMPTY_POSITION;
    node->next = NULL;
    node->previous = NULL;

    return node;
}

/**
 * Frees the queue node and the position associated with it.
 *
 * @param node The node to de-allocate.
 */
static void free_queue_node(QueueNode *node) { free(node); }

void enqueue(Queue *queue, Position position) {
    // Build Queue Node
    QueueNode *new_node = new_queue_node();
    if (new_node == NULL) {
        fprintf(stderr, "Failed to Enqueue Node");
        return;
    }

    // Create Node
    new_node->position.is_empty = false;
    new_node->position.x = position.x;
    new_node->position.y = position.y;

    // Insert into Queue
    if (queue->head == NULL) {
        // First Node, Both Head and Tail
        queue->head = queue->tail = new_node;
    } else {
        // Add to Tail

        // Link New Node
        new_node->next = queue->tail;
        new_node->previous = NULL;

        // Link Old Tail
        queue->tail->previous = new_node;

        // Link Tail
        queue->tail = new_node;
    }
}

Position dequeue(Queue *queue) {
    if (queue->head == NULL) {
        // The queue is empty.
        return EMPTY_POSITION;
    }

    // Get Current Head
    QueueNode *old_head = queue->head;
    Position position = old_head->position;

    // Move Head
    QueueNode *new_head = old_head->previous;

    if (new_head != NULL) {
        // Unlink Old Head
        new_head->next = NULL;
    }

    // Update Queue
    queue->head = new_head;

    free_queue_node(old_head);

    return position;
}

void free_queue(Queue *queue) {
    // Free All Nodes from Head to Tail
    QueueNode *head = queue->head;

    while (head != NULL) {
        QueueNode *previous = head->previous;

        free_queue_node(head);

        head = previous;
    }

    free(queue);
}
