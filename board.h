#ifndef BOARD_H
#define BOARD_H

#include "list.h"
#include <stdio.h>

/**
 * A container for the board along with its dimensions.
 */
typedef struct {
    /**
     * A growable list of rows.
     */
    List *rows;

    /**
     * The length of each row in the array.
     */
    size_t cols;
} Board;

/**
 * Frees a board allocated on the heap.
 *
 * @param board The board to cleanup.
 */
void free_board(Board *board);

/**
 * Prints the state of a board.
 *
 * @param board The board to print.
 */
void print_input_matrix(const Board *board);

/**
 * Pretty prints the board.
 *
 * @param board The board to print.
 */
void print_board(Board *board, List *path);

/**
 * Reads the board from the input file. Returns a list of integer arrays. The
 * origin (0, 0) is at the top left logical corner. It asserts the invariant
 * condition that all lines are the same length will be true. In all cases, the
 * caller is responsible for freeing the returned board.
 *
 * @param file The file to read the board from.
 * @return The allocated board with information about the content on each line.
 */
Board *read_board(FILE *file);

#endif
