#ifndef LIST_H
#define LIST_H

#include <stdlib.h>

/**
 * A structure to hold a growable list.
 */
typedef struct {
    /**
     * The maximum number of elements which can be stored without a
     * re-allocation.
     */
    size_t cap;

    /**
     * The number of meaningful values in the list.
     */
    size_t length;

    /**
     * The array storing the values.
     */
    void **values;
} List;

/**
 * Allocates a new list on the heap.
 *
 * @return An initialized list.
 */
List *new_list();

/**
 * Frees a list.
 *
 * @param list The list to free.
 */
void free_list(List *list);

/**
 * Adds an element to the list, making allocations if needed.
 *
 * @param list The list to add an item to.
 * @param value The value to add to the list.
 * @return A truthy value if ok, otherwise, a falsy value.
 */
int append_list(List *list, void *value);

#endif
