#include "directions.h"

const Direction directions[] = {
    {.offset_y = 1, .offset_x = 0},
    {.offset_y = 0, .offset_x = 1},
    {.offset_y = -1, .offset_x = 0},
    {.offset_y = 0, .offset_x = -1},
};

/// The number of elements in the directions array.
const int directions_count = sizeof(directions) / sizeof(directions[0]);
