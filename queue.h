#ifndef QUEUE_H
#define QUEUE_H

#include "position.h"
#include <stdbool.h>
#include <stdlib.h>

/**
 * A node in the linked queue.
 */
typedef struct QueueNode_s {
    /**
     * The next node closest to the tail.
     */
    struct QueueNode_s *previous;

    /**
     * The next node closest to the head.
     */
    struct QueueNode_s *next;

    Position position;
} QueueNode;

/**
 * A container to hold the state of a linked FIFO queue.
 */
typedef struct {
    QueueNode *head;
    QueueNode *tail;
} Queue;

/**
 * Creates a new queue.
 *
 * @return A pointer to the queue or null if it couldn't be created.
 */
Queue *new_queue();

/**
 * Adds an element to the back of the queue.
 *
 * @param queue The queue to modify.
 * @param position The item to add to the queue.
 */
void enqueue(Queue *queue, Position position);

/**
 * Removes an element from the front of the queue and returns its associated
 * value.
 *
 * @param queue The queue to modify.
 * @return The value of the item at the front of the queue. Returns null if the
 * queue is empty.
 */
Position dequeue(Queue *queue);

/**
 * Frees a queue.
 *
 * @param queue The queue to delete.
 */
void free_queue(Queue *queue);

#endif
