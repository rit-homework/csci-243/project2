#ifndef SEARCH_H
#define SEARCH_H

#include "board.h"
#include "position.h"

/**
 * Finds a way to travel from the top left corner to to bottom right corner
 * using BFS.
 *
 * @param board The board to search for a solution through.
 * @return A list of positions which are part of the solution. They are ordered
 * from the last position to the first position. The caller assumes ownership of
 * the list and it's contents.
 */
List *find_solution(Board *board);

#endif
